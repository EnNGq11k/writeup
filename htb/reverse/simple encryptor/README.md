# Hack the box: Simple encryptor
Writeup for HTB Simple encryptor

## Zip content
The zip contains a executable `encrypt` and a `flag.enc` file.

## Strings

![Strings](strings.png)

There are only two interesting strings: `flag` and `flag.enc`.

## Execution
Execution will quit with a segfault

![sefault](segfault1.png)

Same result with `flag.enc` from the zip as parameter.

![sefault](segfault2.png)

## Disassemble

Load file into `Ghidra` and look at the functions

![functions](functions.png)

As usual `main` should be interesting.

![main](main_org.png)

There`s a lot of code here. Let us rename the variables to get a better view.

![main](main_renamed.png)

Analyze the code

![main](main_renamed_annotated.png)

This program will read the file `flag` applies some `pseudo encryption` or `obfuscation` to every character and writes the result to `flag.enc`. A `flag.enc` is provided in the zip file. This means we have to understand the "encryption", reverse it and apply it to the `flag.enc` to get the flag.

### Open and read flag

That's easy to understand. Open the file `flag` and write it to the buffer.

### Setup rand

https://cplusplus.com/reference/cstdlib/rand/

https://cplusplus.com/reference/cstdlib/srand/

Rand() is a pseudo-random generator. This means, with the same seed (srand(<seed>)) it will always return the same random numbers. There`s one exception: It depends on the implementation of your operating system. To generate the same random numbers, we have to use a Linux operating system.

```c++
#include <iostream>
#include <stdlib.h> /* srand, rand */

int main()
{
    srand(0);
    std::cout << rand() << std::endl;
    std::cout << rand() << std::endl;
    return 0;
}
```

```
% g++ srand_test.cpp -o srandtest
% ./srandtest
1804289383
846930886

% ./srandtest
1804289383
846930886
## Same result for every run
```

As we can see in `line 38` the seed `__time` will be available in the `flag.enc` file.

### Loop
We now apply `XOR` and the `"bit swap"` to every character of the flag.

### XOR
https://en.wikipedia.org/wiki/Bitwise_operation#XOR

A xor operation is always reversible if you know the operand. In our case, this will be the random number. So this should not be a problem

```
#XOR
0x100 ^ 0x010 = 0x110

#Restore original value
0x110 ^ 0x010 = 0x100
```

### Bit swap
https://en.wikipedia.org/wiki/Bitwise_operation#AND

https://en.wikipedia.org/wiki/Bitwise_operation#OR

https://en.wikipedia.org/wiki/Bitwise_operation#Logical_shift

This is a quite creative way to use random and bit shift to obfuscate data.

First we need to know that `sbyte` contains `8 bits`.  Lets take the character `A` as an example:
```
char  hex   binary (bits)
A     0x41  0100 0001
```

The following code will create a random number between 0 and 7

```c++
random_2 = rand();
random_2 = random_2 & 7;
```

Now we can analyse the tricky part. A bit shift just moves the bits to the left or right and fills the rest with zeroes.
```
0001 << 1 = 0010 // shift by 1 to left
1000 >> 2 = 0010 // shift by 2 to right
````

```c++
flag + i =
    flag + i << random_2         // Bit shift left by 0 - 7
    |                            // OR result of bit shift with next line
    flag + i >> (8 - random_2);  // Bit shift right by 1 - 8
```

Lets assume `random_2 & 7` is 4.
```
char | binary    | bit shift left 4 (line 34) | bit shift right 4 (line 35)
A    | 0100 0001 | 0001 0000                  | 0000 0100

// Apply OR (Line 34)

left  0001 0000
right 0000 0100
===============
OR    0001 0100
```

You maybe noticed that the we just switched the first 4 bytes with the last 4 bytes. This means this operation is reversiable.


## Solution

We now understand the encrypt code and we know that the enrcption is reversable. The next step is to write the decryptor.

1. Read flag.enc
2. Extract the first 4 bytes to get the seed for rand()
3. Setup rand with seed
4. Decrypt the remaining content of the file
    * For each charater:
    * Create two random numbers
    * Swap bits with the second random number
    * XOR with first random number
    * Write result


```c++
#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

int main()
{
    // Open file and get length
    FILE *flag_input;
    size_t file_length;
    flag_input = fopen("flag.enc","rb");
    fseek(flag_input, 0, SEEK_END);
    file_length = ftell(flag_input);
    fseek(flag_input, 0, SEEK_SET);

    // Read seed from file
    unsigned int seed = 0;
    fread(&seed, 4, 1, flag_input);
    std::cout << std::hex << "Seed: " << seed << std::endl;

    // Read flag (stating at 4)
    unsigned int flag_length = file_length - 4;
    unsigned char buffer[flag_length];
    fread(buffer,file_length, 1, flag_input);
    fclose(flag_input);

    // Setup rand
    srand(seed);

    std::cout << "Flag: ";
    for (int i = 0; i < flag_length; i++)
    {
        // Setup random
        unsigned int first_random = rand();
        unsigned int second_random = rand();
        second_random = second_random & 7;

        unsigned char current_char = buffer[i];

        // Reset bits. Just reverse bit operations
        current_char = current_char >> second_random |
                       current_char << (8 - second_random);

        // XOR
        current_char = current_char ^ first_random;

        // Print
        std::cout << current_char;
    }

    std::cout << std::endl;
    return 0;
}
```

This will print the flag

![main](flag.png)