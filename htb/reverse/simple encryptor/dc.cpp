#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

int main()
{
    // Open file and get length
    FILE *flag_input;
    size_t file_length;
    flag_input = fopen("flag.enc","rb");
    fseek(flag_input,0,2);
    file_length = ftell(flag_input);
    fseek(flag_input,0,0);
    
    // Read seed from file
    unsigned int seed = 0;
    fread(&seed, 4, 1, flag_input);
    std::cout << std::hex << "Seed: " << seed << std::endl;

    // Read flag (stating at 4)
    unsigned int flag_length = file_length - 4;
    unsigned char buffer[flag_length];
    fread(buffer,file_length,1,flag_input);
    fclose(flag_input);
    
    // Setup rand
    srand(seed);

    std::cout << "Flag: ";
    for (int i = 0; i < flag_length; i++)
    {
        unsigned int first_random = rand();
        unsigned int second_random = rand();
        second_random = second_random & 7;

        unsigned char current_char = buffer[i];

        current_char = current_char >> second_random |
                       current_char << (8 - second_random);

        current_char = current_char ^ first_random;

        std::cout << current_char; 
    }
    
    std::cout << std::endl;
    return 0;
}